Was trying to complete the project using the API Designer in Anypoint Platform. Alas, I got stuck somewhere and lack the time and experience to work through it. Decided to just use Anypoint Studio alone to try to make it in time. I do believe I no longer need the Transform Message component in there and can just direct the payload to the Http Request component, but I'm explearning and just gonna leave it there anyway.

You'll also see in the Error Response of the Http Listener that the errorMessage is hard coded. I think the better way would've been to get the message from the Validator, but I could not figure how.

I didn't include the configurable parameters of the Account REST Api as they were not part of the final requirements, but I can do so upon request.
